﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BumaReportManagement.Helpers
{
    public class SqlServerHelper
    {
        public string connectionString = String.Empty;

        public SqlServerHelper(string connection)
        {
            this.connectionString = connection;
        }

        public SqlConnection SQLConnect()
        {
            var conn = new SqlConnection(connectionString);
            return conn;
        }

        public bool ExecQuery(string query)
        {
            using (var conn = new SqlConnection(connectionString))
            {

                try
                {
                    bool result = false;
                    conn.Open();
                    var trans = conn.BeginTransaction();
                    using (var cmd = new SqlCommand(query, conn, trans))
                    {
                        if (cmd.ExecuteNonQuery() >= 0)
                        {
                            trans.Commit();
                            result = true;
                        }
                        else
                        {
                            trans.Rollback();
                            result = false;
                        }
                    }
                    conn.Close();
                    conn.Dispose();
                    return result;
                }
                catch (Exception ex)
                {
                    conn.Close();
                    conn.Dispose();
                    throw new Exception(ex.Message);
                }
            }
        }


        public bool ExecQueryNoRollback(string query)
        {
            using (var conn = new SqlConnection(connectionString))
            {
                try
                {
                    bool result = false;
                    conn.Open();
                    var trans = conn.BeginTransaction();
                    using (var cmd = new SqlCommand(query, conn, trans))
                    {
                        if (cmd.ExecuteNonQuery() >= 0)
                        {
                            trans.Commit();
                            result = true;
                        }
                        else
                        {
                            trans.Commit();
                            //trans.Rollback();
                            result = false;
                        }
                    }
                    conn.Close();
                    conn.Dispose();
                    return result;
                }
                catch (Exception ex)
                {
                    conn.Close();
                    conn.Dispose();
                    throw new Exception(ex.Message);
                }
            }
        }


        public DataTable ExecReader(string query)
        {
            using (var conn = new SqlConnection(connectionString))
            {
                try
                {
                    DataTable datatable = new DataTable();
                    conn.Open();
                    using (var cmd = new SqlCommand(query, conn))
                    {
                        cmd.CommandTimeout = 2000;
                        var reader = cmd.ExecuteReader();
                        datatable.Load(reader);
                    }
                    conn.Close();
                    conn.Dispose();
                    return datatable;
                }
                catch (Exception ex)
                {
                    conn.Close();
                    conn.Dispose();
                    throw new Exception(ex.Message);
                }
            }
        }

        public bool ExecStoredProcedure(string spName)
        {
            using (var conn = new NpgsqlConnection(connectionString))
            {
                try
                {
                    bool result = true;
                    DataTable datatable = new DataTable();
                    conn.Open();
                    using (var cmd = new NpgsqlCommand(spName, conn))
                    {
                        cmd.CommandTimeout = 2000;
                        cmd.CommandType = CommandType.StoredProcedure;
                        NpgsqlDataReader reader = cmd.ExecuteReader();
                    }
                    conn.Close();
                    conn.Dispose();
                    return result;
                }
                catch (Exception ex)
                {
                    conn.Close();
                    conn.Dispose();
                    throw new Exception(ex.Message);
                }
            }
        }


        public DataTable UploadDataTable(string spName, DataTable table, string userUpload, out int status)
        {
            using (var conn = new SqlConnection(connectionString))
            {
                try
                {
                    status = 0;
                    bool result = true;
                    DataTable datatable = new DataTable();
                    conn.Open();
                    using (var cmd = new SqlCommand(spName, conn))
                    {
                        cmd.CommandTimeout = 2000;
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@DataHMList", SqlDbType.Structured);
                        cmd.Parameters["@DataHMList"].Value = table;

                        cmd.Parameters.Add("@UserUpload", SqlDbType.VarChar, 100);
                        cmd.Parameters["@UserUpload"].Value = userUpload;

                        cmd.Parameters.Add("@Status", SqlDbType.Int, 100);
                        cmd.Parameters["@Status"].Value = status;
                        cmd.Parameters["@Status"].Direction = ParameterDirection.Output;

                        SqlDataReader reader = cmd.ExecuteReader();
                        //var abc = cmd.v
                    }
                    conn.Close();
                    conn.Dispose();
                }
                catch (Exception ex)
                {
                    conn.Close();
                    conn.Dispose();
                    throw new Exception(ex.Message);
                }
            }

            return null;
        }

    }
}