﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace BumaReportManagement.Helpers
{
    public static class GlobalVar
    {
        public static string _NpgsqlConnectionString = ConfigurationManager.ConnectionStrings["NPGSQLEntities"].ConnectionString;
        public static string _SqlServerConnectionString = ConfigurationManager.ConnectionStrings["SQLServerEntities"].ConnectionString;

        public static string _NpgsqlSchema_rds = ConfigurationManager.AppSettings["NpgsqlSchema_rds"];
        public static string _NpgsqlSchema_mpr = ConfigurationManager.AppSettings["NpgsqlSchema_mpr"];
        public static string _NpgsqlSchema_sap = ConfigurationManager.AppSettings["NpgsqlSchema_sap"];
        public static string _NpgsqlSchema_sapbdm = ConfigurationManager.AppSettings["NpgsqlSchema_sapbdm"];
        public static string _NpgsqlSchema_mcd = ConfigurationManager.AppSettings["NpgsqlSchema_mcd"];
        public static string _NpgsqlSchema_sql = ConfigurationManager.AppSettings["NpgsqlSchema_sql"];
    }
}