﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BumaReportManagement.Helpers
{
    public class NpgsqlHelper
    {
        string connectionString = String.Empty;

        public NpgsqlHelper(string connection)
        {
            this.connectionString = connection;
        }

        public NpgsqlConnection GPConnect()
        {
            NpgsqlConnection conn = new NpgsqlConnection(connectionString);
            return conn;
        }

        public bool ExecQuery(string query)
        {
            using (var conn = new NpgsqlConnection(connectionString))
            {

                try
                {
                    bool result = false;
                    conn.Open();
                    NpgsqlTransaction trans = conn.BeginTransaction();
                    using (var cmd = new NpgsqlCommand(query, conn, trans))
                    {
                        if (cmd.ExecuteNonQuery() >= 0)
                        {
                            trans.Commit();
                            result = true;
                        }
                        else
                        {
                            trans.Rollback();
                            result = false;
                        }
                    }
                    conn.Close();
                    conn.Dispose();
                    return result;
                }
                catch (Exception ex)
                {
                    conn.Close();
                    conn.Dispose();
                    throw new Exception(ex.Message);
                }
            }
        }


        public bool ExecQueryNoRollback(string query)
        {
            using (var conn = new NpgsqlConnection(connectionString))
            {
                try
                {
                    bool result = false;
                    conn.Open();
                    NpgsqlTransaction trans = conn.BeginTransaction();
                    using (var cmd = new NpgsqlCommand(query, conn, trans))
                    {
                        if (cmd.ExecuteNonQuery() >= 0)
                        {
                            trans.Commit();
                            result = true;
                        }
                        else
                        {
                            trans.Commit();
                            //trans.Rollback();
                            result = false;
                        }
                    }
                    conn.Close();
                    conn.Dispose();
                    return result;
                }
                catch (Exception ex)
                {
                    conn.Close();
                    conn.Dispose();
                    throw new Exception(ex.Message);
                }
            }
        }


        public DataTable ExecReader(string query)
        {
            using (var conn = new NpgsqlConnection(connectionString))
            {
                try
                {
                    DataTable datatable = new DataTable();
                    conn.Open();
                    using (var cmd = new NpgsqlCommand(query, conn))
                    {
                        cmd.CommandTimeout = 2000;
                        NpgsqlDataReader reader = cmd.ExecuteReader();
                        datatable.Load(reader);
                    }
                    conn.Close();
                    conn.Dispose();
                    return datatable;
                }
                catch (Exception ex)
                {
                    conn.Close();
                    conn.Dispose();
                    throw new Exception(ex.Message);
                }
            }
        }

        public bool ExecStoredProcedure(string spName)
        {
            using (var conn = new NpgsqlConnection(connectionString))
            {
                try
                {
                    bool result = true;
                    DataTable datatable = new DataTable();
                    conn.Open();
                    using (var cmd = new NpgsqlCommand(spName, conn))
                    {
                        cmd.CommandTimeout = 2000;
                        cmd.CommandType = CommandType.StoredProcedure;
                        NpgsqlDataReader reader = cmd.ExecuteReader();
                    }
                    conn.Close();
                    conn.Dispose();
                    return result;
                }
                catch (Exception ex)
                {
                    conn.Close();
                    conn.Dispose();
                    throw new Exception(ex.Message);
                }
            }
        }
        
    }
}