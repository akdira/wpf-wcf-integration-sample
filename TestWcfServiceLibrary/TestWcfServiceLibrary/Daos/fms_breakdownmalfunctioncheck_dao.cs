﻿using BumaReportManagement.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace TestWcfServiceLibrary.Daos
{
    public class User_dao
    {

        SqlServerHelper hNpgsql;
        string schemaRds = String.Empty;
        string schemaSap = String.Empty;
        string table = String.Empty;


        public User_dao()
        {
            hNpgsql = new SqlServerHelper(GlobalVar._SqlServerConnectionString);
            schemaRds = GlobalVar._NpgsqlSchema_rds;
            schemaSap = GlobalVar._NpgsqlSchema_sap;
            table = "FMS_BreakdownMalfunctionCheck";
        }

        public DataTable BdStatusListDt()
        {
            var query = $@"SELECT *
                           FROM BD_Status
                           WHERE IsEnable = 1
                           ORDER BY try_cast(ID_Status AS int) ASC, ID_Status ASC";

            return hNpgsql.ExecReader(query);

        }

        public DataTable McdNotifStatusListDt()
        {
            var query = $@"select
		                      DISTINCT br.notification_status AS notif_status
	                        from [172.16.36.36].[FMSLiteDB].[dbo].mcc_brkmtr_trbreakdown br
	                        WHERE COALESCE(br.notification_status,'') <> ''
	                        ORDER BY br.notification_status ASC";

            return hNpgsql.ExecReader(query);

        }

        public DataTable SapNotifStatusListDt()
        {
            var query = $@"select 
		                   DISTINCT hd.ntfstat as notif_status
	                       from [HANA].[HANA].zidmbd hd
	                       where hd.isdeleted <> 'X' AND COALESCE(hd.ntfstat,'') <> ''
	                       ORDER BY hd.ntfstat ASC";

            return hNpgsql.ExecReader(query);

        }


        public DataTable GetAll(
            string SiteID,
            DateTime FromDate,
            DateTime ToDate,
            out int totalCount,
            out int completePercent,
            out int completeCount,
            out int sapNocoIsClosed,
            out int mcdFailedNoco,
            out int malfEndDiff,
            out int hmMappingBlank,
            out int errorCount,
            List<string> equipmentList = null,
            string notification = "",
            List<string> mcdNotifStatusList = null,
            List<string> sapNotifStatusList = null,
            List<string> statusList = null

            )
        {


            // Begin Where


            var where = "1=1";

            // Equipment
            if (equipmentList.Count > 0)
            {
                where += $@" AND (";
                var equipmentCount = 0;

                foreach (var oneEquipment in equipmentList)
                {
                    equipmentCount++;
                    if (equipmentCount > 1) where += $@" OR ";

                    where += $@" mcd_equipment = '{oneEquipment}' ";
                }

                where += $@" ) ";
            }

            // McdNotifStatusList
            if (mcdNotifStatusList.Count > 0)
            {
                where += $@" AND (";
                var mcdNotifStatusCount = 0;

                foreach (var oneMcdNotifStatus in mcdNotifStatusList)
                {
                    mcdNotifStatusCount++;
                    if (mcdNotifStatusCount > 1) where += $@" OR ";

                    where += $@" [mcd_notif_status] = '{oneMcdNotifStatus}' ";
                }

                where += $@" ) ";
            }

            // SapNotifStatusList
            if (sapNotifStatusList.Count > 0)
            {
                where += $@" AND (";
                var sapNotifStatusCount = 0;

                foreach (var oneMcdNotifStatus in sapNotifStatusList)
                {
                    sapNotifStatusCount++;
                    if (sapNotifStatusCount > 1) where += $@" OR ";

                    where += $@" [sap_notif_status] = '{oneMcdNotifStatus}' ";
                }

                where += $@" ) ";
            }


            // StatusList
            if (statusList.Count > 0)
            {
                where += $@" AND (";
                var statusCount = 0;

                foreach (var oneStatus in statusList)
                {
                    statusCount++;
                    if (statusCount > 1) where += $@" OR ";

                    where += $@" [Status] = '{oneStatus}' ";
                }

                where += $@" ) ";
            }

            // Notification
            if (!string.IsNullOrWhiteSpace(notification))
                where += $@" AND [notification] LIKE '%{notification.Trim()}%'  ";


            // End Where

            using (var conn = new SqlConnection(hNpgsql.connectionString))
            {
                try
                {
                    //status = 0;
                    bool result = true;
                    var spName = "FMS_BreakdownMalfunctionCheckModified";
                    DataTable datatable = new DataTable();
                    conn.Open();
                    using (var cmd = new SqlCommand(spName, conn))
                    {
                        cmd.CommandTimeout = 2000;
                        cmd.CommandType = CommandType.StoredProcedure;

                        //@SiteID = N'2004',
                        //@StartDate = N'20190122',
                        //@EndDate = N'20190122',
                        //@strwhere = N'equipment=''CT7265''',

                        //@TotalCount int,
                        //@CompletePercent int,
                        //@CompleteCount int,
                        //@SapNocoIsClosed int,
                        //@McdFailedNoco int,
                        //@MalfEndDiff int,
                        //@HmMappingBlank int


                        SqlParameter a;
                        a = cmd.Parameters.Add("@SiteID", SqlDbType.VarChar);
                        a.Direction = ParameterDirection.Input;
                        a.Value = SiteID;

                        a = cmd.Parameters.Add("@StartDate", SqlDbType.VarChar);
                        a.Direction = ParameterDirection.Input;
                        a.Value = FromDate.ToString("yyyyMMdd");

                        a = cmd.Parameters.Add("@EndDate", SqlDbType.VarChar);
                        a.Direction = ParameterDirection.Input;
                        a.Value = ToDate.ToString("yyyyMMdd");

                        a = cmd.Parameters.Add("@strwhere", SqlDbType.VarChar);
                        a.Direction = ParameterDirection.Input;
                        a.Value = where;

                        //cmd.Parameters.Add("@EndDate", SqlDbType.VarChar).Direction = ParameterDirection.Input;
                        //cmd.Parameters.Add("@strwhere", SqlDbType.VarChar).Direction = ParameterDirection.Input;

                        cmd.Parameters.Add("@TotalCount", SqlDbType.Int).Direction = ParameterDirection.Output;
                        cmd.Parameters.Add("@CompletePercent", SqlDbType.Int).Direction = ParameterDirection.Output;
                        cmd.Parameters.Add("@CompleteCount", SqlDbType.Int).Direction = ParameterDirection.Output;
                        cmd.Parameters.Add("@SapNocoIsClosed", SqlDbType.Int).Direction = ParameterDirection.Output;
                        cmd.Parameters.Add("@McdFailedNoco", SqlDbType.Int).Direction = ParameterDirection.Output;
                        cmd.Parameters.Add("@MalfEndDiff", SqlDbType.Int).Direction = ParameterDirection.Output;
                        cmd.Parameters.Add("@HmMappingBlank", SqlDbType.Int).Direction = ParameterDirection.Output;
                        cmd.Parameters.Add("@ErrorCount", SqlDbType.Int).Direction = ParameterDirection.Output;

                        //cmd.Parameters.Add("@Status", SqlDbType.Int, 100);
                        //cmd.Parameters["@Status"].Value = status;
                        //cmd.Parameters["@Status"].Direction = ParameterDirection.Output;

                        SqlDataReader reader = cmd.ExecuteReader();


                        //@TotalCount int,
                        //@CompletePercent int,
                        //@CompleteCount int,
                        //@SapNocoIsClosed int,
                        //@McdFailedNoco int,
                        //@MalfEndDiff int,
                        //@HmMappingBlank int

                        datatable.Load(reader);

                        totalCount = Convert.ToInt32(cmd.Parameters["@TotalCount"].Value);
                        completePercent = Convert.ToInt32(cmd.Parameters["@CompletePercent"].Value);

                        completeCount = Convert.ToInt32(cmd.Parameters["@CompleteCount"].Value);
                        sapNocoIsClosed = Convert.ToInt32(cmd.Parameters["@SapNocoIsClosed"].Value);
                        mcdFailedNoco = Convert.ToInt32(cmd.Parameters["@McdFailedNoco"].Value);
                        malfEndDiff = Convert.ToInt32(cmd.Parameters["@MalfEndDiff"].Value);
                        hmMappingBlank = Convert.ToInt32(cmd.Parameters["@HmMappingBlank"].Value);
                        errorCount = Convert.ToInt32(cmd.Parameters["@ErrorCount"].Value);


                        conn.Close();
                        conn.Dispose();

                        return datatable;
                    }
                }
                catch (Exception ex)
                {
                    conn.Close();
                    conn.Dispose();
                    throw new Exception(ex.Message);
                }
            }
        }

        public DataTable GetAllEquipment()
        {
            var query = $@"SELECT a.mcd_equipment equipment
                           from [172.16.36.36].[FMSLiteDB].[dbo].mcc_fis_mpequipmentmapping a
                           WHERE a.mcd_equipment <> ''
                           GROUP BY a.mcd_equipment
                           ORDER BY a.mcd_equipment";

            return hNpgsql.ExecReader(query);
        }

        public DataTable GetAll(string SiteID, DateTime FromDate, DateTime ToDate, List<string> equipmentList = null, string notification = "")
        {
            if (equipmentList == null) equipmentList = new List<string>();
            string query = String.Empty;

            var where = "1=1";

            if (equipmentList.Count > 0)
            {
                where += $@" AND (";
                var equipmentCount = 0;

                foreach (var oneEquipment in equipmentList)
                {
                    equipmentCount++;
                    if (equipmentCount > 1) where += $@" OR ";

                    where += $@" mcd_equipment = ''{oneEquipment}'' ";
                }

                where += $@" ) ";
            }

            if (!string.IsNullOrWhiteSpace(notification))
            {
                where += $@" AND notification LIKE ''%{notification.Trim()}%''  ";
            }

            query = $@"
                        EXEC FMS_BreakdownMalfunctionCheckModified '{SiteID}', '{FromDate.ToString("yyyyMMdd")}', '{ToDate.ToString("yyyyMMdd")}', '{where}'
                      ";

            return hNpgsql.ExecReader(query);
        }

    }
}